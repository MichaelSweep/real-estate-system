<button class="button-secondary upload_image_button" data-title="<?php _e( 'Select images for offer' ); ?>" data-btntext="<?php _e( 'Insert' ); ?>">
	<span class="dashicons dashicons-images-alt2"></span>
	<?php _e( 'Upload Images' ); ?>
</button>

<?php global $post;
$imagesIds = get_post_meta( $post->ID, 'res_offer_images', true ); ?>
<div class="thumbs-prev">
	<?php if ($imagesIds != '') : ?>
		<?php foreach ($imagesIds as $index=>$id): ?>
            <?php $urlImage = wp_get_attachment_image_src( $id, 'thumbnail' );?>
			<div>
                <input type="hidden" name="res_offer_images[<?=$id?>]" value="<?=$id?>">
                <img src="<?=$urlImage[0]?>">
                <span class="dashicons dashicons-dismiss"></span>
            </div>
        <?php endforeach; ?>
	<?php endif; ?>
</div>

<script>

    // Media Uploader
    var res_offer_images;

    jQuery('.upload_image_button').live('click', function( event ){

        event.preventDefault();

        var this_widget = jQuery(this).closest('tr');


        // Create the media frame.
        res_offer_images = wp.media.frames.res_offer_images = wp.media({
            title: jQuery( this ).data( 'title' ),
            button: {
                text: jQuery( this ).data( 'btntext' ),
            },
            multiple: false  // Set to true to allow multiple files to be selected
        });

        // When an image is selected, run a callback.
        res_offer_images.on( 'select', function() {
            // We set multiple to false so only get one image from the uploader
            var selection = res_offer_images.state().get('selection');
            selection.map( function( attachment ) {
                attachment = attachment.toJSON();
                jQuery('.thumbs-prev').append('<div><input type="hidden" name="res_offer_images['+attachment.id+']" value="'+attachment.id+'"><img src="'+attachment.url+'"><span class="dashicons dashicons-dismiss"></span></div>');
            });
        });

        // Finally, open the modal
        res_offer_images.open();
    });

    jQuery('.thumbs-prev').on('click', '.dashicons-dismiss', function() {
        jQuery(this).parent('div').remove();
    });
</script>