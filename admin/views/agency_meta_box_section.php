<div class="form-field">
    <? foreach ($fields as $key=>$field) : ?>

	    <?php $value = get_term_meta($_GET['tag_ID'], RES_META_BOX_PREFIX.$key, true); ?>
        <label for="<?=$key?>"><?php _e($field['title']); ?></label>

        <? Helpers::render_field($field,$key,$value); ?>
    <? endforeach; ?>
</div>

<?php wp_nonce_field( 'my_agency_meta_box_section_nonce', 'my_agency_meta_box_section_nonce' ); ?>