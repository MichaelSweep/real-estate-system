<div class="form-group">
    <div class="col-sm-3">
        <button class="btn" type="button" id="add_feature">Add</button>
    </div>
    <div class="col-sm-9 features_list">
        <? if(is_array($values)) : ?>
	        <?php foreach ($values as $index => $value) : ?>
                <div class="feature_item">
                    <input class="form-control input-sm"
                           type="text"
                           name="<?=RES_META_BOX_PREFIX.'features[]'?>"
                           value="<?=$value?>">
                    <a onclick="delete_feature_event(jQuery(this))">Delete</a>
                </div>
	        <?php endforeach; ?>
        <? else : ?>
            <div class="feature_item">
                <input class="form-control input-sm"
                       type="text"
                       name="<?=RES_META_BOX_PREFIX.'features[]'?>"
                       value="">
                <a onclick="delete_feature_event(jQuery(this))">Delete</a>
            </div>
        <? endif; ?>
    </div>
    <div class="clearfix"></div>
</div>