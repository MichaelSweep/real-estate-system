 <?php
$drag_icon = apply_filters( 'res_maps_drag_icon', RES_URL.'/assets/pin-drag.png' );
$maps_api = apply_filters( 'res_maps_api', 'AIzaSyBbpbij9IIXGftKhFLMHOuTpAbFoTU_8ZQ' );
global $post;
$def_lat = Helpers::RES_get_option('default_map_latitude', '-6.404923058371801');
$def_long = Helpers::RES_get_option('default_map_longitude', '145.31503509194954');
$zoom_level = Helpers::RES_get_option('map_zoom', '8');

if (isset($post->ID) && get_post_meta( $post->ID, 'res_latitude', true ) != '') {
	$def_lat = get_post_meta( $post->ID, 'res_latitude', true );
}

if (isset($post->ID) && get_post_meta( $post->ID, 'res_longitude', true ) != '') {
	$def_long = get_post_meta( $post->ID, 'res_longitude', true );
}
?>
<div class="ich-settings-main-wrap">
	<input type="text" class="form-control enter_no_submit" id="search-map" placeholder="<?php _e( 'Type to Search...', 'real-estate-manager' ); ?>" onkeypress="return event.keyCode != 13;">
    <input type="hidden" id="">
	<div id="map-canvas" style="height: 300px"></div>
	<?php if (is_ssl()) { ?>
		<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $maps_api; ?>&libraries=places"></script>
	<?php } else { ?>
		<script src="http://maps.googleapis.com/maps/api/js?key=<?php echo $maps_api; ?>&libraries=places"></script>
	<?php } ?>
	<script>
        function initialize() {

            var map = new google.maps.Map(document.getElementById('map-canvas'), {
                center: new google.maps.LatLng(<?php echo $def_lat; ?>, <?php echo $def_long; ?>),
                scrollwheel: false,
                zoom: <?php echo $zoom_level; ?>
            });

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(<?php echo $def_lat; ?>, <?php echo $def_long; ?>),
                map: map,
                icon: '<?php echo $drag_icon; ?>',
                draggable: true
            });

            google.maps.event.addListener(marker, 'drag', function(event) {
                jQuery('#latitude').val(event.latLng.lat());
                jQuery('#longitude').val(event.latLng.lng());
                jQuery('#position').text('Position: ' + event.latLng.lat() + ' , ' + event.latLng.lng() );
            });
            google.maps.event.addListener(marker, 'dragend', function(event) {
                jQuery('#latitude').val(event.latLng.lat());
                jQuery('#longitude').val(event.latLng.lng());
                jQuery('#position').text('Position: ' + event.latLng.lat() + ' , ' + event.latLng.lng() );
            });


            var searchBox = new google.maps.places.SearchBox(document.getElementById('search-map'));
            // map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById('search-map'));
            google.maps.event.addListener(searchBox, 'places_changed', function() {
                searchBox.set('map', null);


                var places = searchBox.getPlaces();

                var bounds = new google.maps.LatLngBounds();
                var i, place;
                for (i = 0; place = places[i]; i++) {
                    (function(place) {
                        var marker = new google.maps.Marker({
                            position: place.geometry.location,
                            map: map,
                            icon: '<?php echo $drag_icon; ?>',
                            draggable: true
                        });
                        var location = place.geometry.location;
                        var n_lat = location.lat();
                        var n_lng = location.lng();
                        jQuery('#latitude').val(n_lat);
                        jQuery('#longitude').val(n_lng);
                        jQuery('#position').text('Position: ' + n_lat + ' , ' + n_lng );
                        marker.bindTo('map', searchBox, 'map');
                        google.maps.event.addListener(marker, 'map_changed', function(event) {
                            if (!this.getMap()) {
                                this.unbindAll();
                            }
                        });
                        google.maps.event.addListener(marker, 'drag', function(event) {
                            jQuery('#latitude').val(event.latLng.lat());
                            jQuery('#longitude').val(event.latLng.lng());
                            jQuery('#position').text('Position: ' + event.latLng.lat() + ' , ' + event.latLng.lng() );
                        });
                        google.maps.event.addListener(marker, 'dragend', function(event) {
                            jQuery('#latitude').val(event.latLng.lat());
                            jQuery('#longitude').val(event.latLng.lng());
                            jQuery('#position').text('Position: ' + event.latLng.lat() + ' , ' + event.latLng.lng() );
                        });
                        bounds.extend(place.geometry.location);


                    }(place));

                }
                map.fitBounds(bounds);
                searchBox.set('map', map);
                map.setZoom(Math.min(map.getZoom(), <?php echo $zoom_level; ?>));

            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
	</script>
	<br>
	<div id="position" class="alert alert-info">
		<?php
		_e( 'Search the address on search bar. ', 'real-estate-manager' );
		_e( 'Drag the pin to the location on the map', 'real-estate-manager' );
		?>
	</div>
</div>