<?php foreach ($fields as $key => $field) : ?>

    <?php $value = get_post_meta($post->ID, RES_META_BOX_PREFIX.$key, true); ?>

    <div class="form-group">
        <label for="<?=$key?>" class="col-sm-3 control-label">
	        <?php echo $field['title'] ?>
        </label>
        <div class="col-sm-12">
            <? Helpers::render_field($field,$key,$value); ?>
        </div>
        <div class="clearfix"></div>
    </div>

<?php endforeach; ?>

<?php wp_nonce_field( 'my_main_meta_box_section_nonce', 'main_meta_box_section_nonce' ); ?>