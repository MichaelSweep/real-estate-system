<?php


/**
 * Get terms for a user and a taxonomy
 *
 * @since 0.1.0
 * @param  mixed  $user
 * @param  int    $taxonomy
 * @return mixed
 */
function agency_get_terms_for_user( $user = false, $taxonomy = '' ) {

	// Verify user ID
	$user_id = is_object( $user )
		? $user->ID
		: absint( $user );

	// Bail if empty
	if ( empty( $user_id ) ) {
		return false;
	}

	// Return user terms
	return wp_get_object_terms( $user_id, $taxonomy, array(
		'fields' => 'all_with_object_id'
	) );
}

/**
 * Save taxonomy terms for a specific user
 *
 * @since 1.0.0
 * @param  int      $user_id
 * @param  string   $taxonomy
 * @param  array    $terms
 * @param  boolean  $bulk
 * @return boolean
 */
function agency_set_terms_for_user( $user_id, $taxonomy, $terms = array(), $bulk = false ) {
	$tax = get_taxonomy( $taxonomy );
	if ( ! current_user_can( 'edit_user', $user_id ) || ! current_user_can( $tax->cap->assign_terms ) ) {
		return false;
	}
	if ( empty( $terms ) && empty( $bulk ) ) {
		$terms = isset( $_POST[ $taxonomy ] )
			? $_POST[ $taxonomy ]
			: null;
	}
	if ( is_null( $terms ) || empty( $terms ) ) {
		wp_delete_object_term_relationships( $user_id, $taxonomy );
	} else {
		$_terms = array_map( 'sanitize_key', $terms );
		wp_set_object_terms( $user_id, $_terms, $taxonomy, false );
	}
	clean_object_term_cache( $user_id, $taxonomy );
}

/**
 * Get all user agencies
 *
 * @since 1.0.0
 * @param array  $args
 * @param string $output
 * @param string $operator
 * @return array
 */
function get_user_agencies( $args = array(), $output = 'names', $operator = 'and' ) {

	// Parse arguments
	$r = wp_parse_args( $args, array(
		'user_group' => true
	) );

	// Return user group taxonomies
	return get_taxonomies( $r, $output, $operator );
}

/**
 * Get all user agency objects
 *
 * @since 1.0.0
 * @param  array  $args
 * @param  string $operator
 * @return array
 */
function get_user_agency_objects( $args = array(), $operator = 'and' ) {
	return get_user_agencies( $args, 'objects', $operator );
}

/**
 * Return a list of users in a specific agency
 *
 * @since 1.0.0
 */
function get_users_of_agency( $args = array() ) {

	$r = wp_parse_args( $args, array(
		'taxonomy' => 'user-group',
		'term'     => '',
		'term_by'  => 'slug'
	) );

	$term     = get_term_by( $r['term_by'], $r['term'], $r['taxonomy'] );
	$user_ids = get_objects_in_term( $term->term_id, $r['taxonomy'] );

	if ( empty( $term ) || empty( $user_ids ) ) {
		return array();
	}

	return get_users( array(
		'orderby' => 'display_name',
		'include' => $user_ids,
	) );
}
