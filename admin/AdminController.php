<?php

require_once( RES_PATH.'/core/Helpers.php' );
require_once( RES_PATH.'/core/Arrays.php' );

class AdminController {


	// options
	const SETTINGS_OPTION = 'res_settings';

    /**
     * Generate admin page and return it as string
     *
     * @return string
     * @since 1.0.0
     */
    static function get_and_save_settings_page()
    {
	    if (isset($_POST['res_settings_nonce'])) {
		    if( !isset( $_POST['res_settings_nonce'] ) || !wp_verify_nonce( $_POST['res_settings_nonce'], 'res_settings_nonce' ) )
			    return 'nonce not verified';

		    $settings_data = $_POST;
		    $settings_data = Helpers::remove_from_array_keys(['post_type', 'page', 'res_settings_nonce', '_wp_http_referer'], $settings_data);
		    update_option( self::SETTINGS_OPTION, $settings_data );
	    }

	    $fields = Arrays::res_settings_array();

	    return include (RES_PATH . "/admin/views/settings_page.php");
    }

	/**
	 * Generate meta box and return it as string
	 *
	 * @return string
	 * @since 1.0.0
	 */
	static function get_main_meta_box_section()
	{
		global $post;

		$fields = Arrays::main_meta_box_array();

		return include (RES_PATH . "/admin/views/main_meta_box_section.php");
	}

	/**
	 * Generate meta box and return it as string
	 *
	 * @return string
	 * @since 1.0.0
	 */
	static function get_features_meta_box_section()
	{
		global $post;

		$values = get_post_meta($post->ID, RES_META_BOX_PREFIX.'features', true);

		return include (RES_PATH . "/admin/views/features_meta_box_section.php");
	}

	/**
	 * Generate meta box and return it as string
	 *
	 * @return string
	 * @since 1.0.0
	 */
	static function get_map_meta_box_section()
	{
		global $post;

		$fields = Arrays::main_meta_box_array();

		return include (RES_PATH . "/admin/views/map_meta_box_section.php");
	}

	/**
	 * Generate meta box and return it as string
	 *
	 * @return string
	 * @since 1.0.0
	 */
	static function get_photos_meta_box_section()
	{
		global $post;

		return include (RES_PATH . "/admin/views/photos_meta_box_section.php");
	}

	/**
	 * Generate meta box and return it as string
	 *
	 * @return string
	 * @since 1.0.0
	 */
	static function get_agency_meta_box_section()
	{
		global $post;

		return include (RES_PATH . "/admin/views/agency_meta_box_section.php");
	}


}