<?php

require_once( RES_PATH.'/core/Helpers.php' );
require_once( RES_PATH.'/core/Arrays.php' );
require_once( RES_PATH.'/core/RegisterRole.php' );
require_once( RES_PATH.'/admin/agencies/AgencyTaxonomy.php' );

class RegisterAgency
{

	const SETTINGS_OPTION = 'res_settings';

	/**
	 * Class plugin RegisterAgency constructor
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function __construct()
	{
		add_action('offer-agency_edit_form_fields', array ( $this, 'offer_agency_edit_form_fields'));
		add_action('offer-agency_add_form_fields',  array ( $this, 'offer_agency_edit_form_fields'));
		add_action('created_offer-agency', array($this, 'offer_agency_save_form'));
		add_action('edited_offer-agency', array($this, 'offer_agency_save_form'));
	}

	function offer_agency_save_form ($term_id)
	{
		foreach (Arrays::res_agency_array() as $key=>$field)
		{
			Helpers::RES_crud_agencies_meta_box($key,$term_id);
		}
	}

	function offer_agency_edit_form_fields () {
		$fields = Arrays::res_agency_array();
		global $post;

		require_once (RES_PATH.'/admin/views/agency_meta_box_section.php');
	}

	/**
	 * Add new user taxonomy "offer-agency"
	 *
	 * @since 1.0.0
	 * @return  void
	 */
	public function register_agency()
	{
		new AgencyTaxonomy(
			'offer-agency',
				'agency',
				array(
					'public'    => true,
					'singular'  => __( 'Agency'),
					'plural'    => __( 'Agencies' )
				)
		);
	}

	/**
	 * Add assets for agencies
	 *
	 * @since 1.0.0
	 * @return  void
	 */
	function wp_user_groups_admin_assets()
	{
		wp_enqueue_style( 'res_offer_agency', RES_URL. '/admin/agencies/css/offer-agency.css');
	}


	/**
	 * Add new section to User Profiles
	 *
	 * @since 1.0.0
	 * @param array $sections
	 */
	function wp_user_groups_add_profile_section( $sections = array() ) {
		$new_sections = $sections;

		$new_sections['groups'] = array(
			'id'    => 'agencies',
			'slug'  => 'agencies',
			'name'  => esc_html__( 'Agencies' ),
			'cap'   => 'edit_profile',
			'icon'  => 'dashicons-groups',
			'order' => 90
		);

		return apply_filters( 'wp_user_groups_add_profile_section', $new_sections, $sections );
	}

}