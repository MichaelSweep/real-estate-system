<?php

class RegisterRole {

    // tables
    const SETTINGS_TABLE    = 'wp_real_estate_system_settings';
	const SETTINGS_OPTION   = 'res_settings';

    /**
     * Class plugin RegisterRole constructor
     *
     * @since 1.0.0
     * @return void
     */
    public function __construct()
    {
	    // add_action( 'admin_init', array($this, 'add_offer_agent_role'));
    }

	/**
	 * Adding offer agent role
	 *
	 * @since 1.0.0
	 * @return void
	 */
    static function add_offer_agent_role()
    {
	    add_role(
		    'offer_agent',
		    __( 'Offer agent' ),
		    array()
	    );
    }

	/**
	 * Adding offer agent role capabilities to create, read, update, delete offers
	 *
	 * @since 1.0.0
	 * @return void
	 */
    static function set_role_capabilities()
    {
		    $offer_agent = get_role('offer_agent');
		    $administrator = get_role('administrator');

		    foreach (unserialize (AGENT_CAPS) as $cap)
		    {
			    $offer_agent->add_cap($cap);
			    $administrator->add_cap($cap);
		    }
    }

	/**
	 * Removing offer agent role capabilities to create, read, update, delete offers
	 *
	 * @since 1.0.0
	 * @return void
	 */
    static function remove_role_capabilities()
    {
	    $offer_agent = get_role('offer_agent');
	    $administrator = get_role('administrator');

	    foreach (unserialize (AGENT_CAPS) as $cap)
	    {
		    $offer_agent->remove_cap($cap);
		    $administrator->remove_cap($cap);
	    }
    }

}