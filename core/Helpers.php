<?php

class Helpers {

	static function RES_crud_offers_meta_box($key,$field)
	{
		global $post;
		$post_id = $post->ID;
		if( isset( $_POST[$key] ) ){
			$crud_data = $_POST[$key];

			// sinitize array
			if(is_array($crud_data))
			{
				$crud_data = array_map( 'sanitize_text_field', $crud_data );
			}

			// save data
			update_post_meta( $post_id, RES_META_BOX_PREFIX . $key, $crud_data );
		}else{
			// delete data
			delete_post_meta( $post_id, RES_META_BOX_PREFIX . $key );
		}
	}

	static function RES_crud_agencies_meta_box($key, $term_id)
	{
		if( isset( $_POST[$key] ) ){
			$crud_data = $_POST[$key];

			// sinitize array
			if(is_array($crud_data))
			{
				$crud_data = array_map( 'sanitize_text_field', $crud_data );
			}

			// save data
			update_term_meta($term_id, RES_META_BOX_PREFIX.$key, $crud_data );
		}else{
			// delete data
			delete_term_meta( $term_id, RES_META_BOX_PREFIX . $key );
		}
	}

	static function RES_crud_offers_photos_meta_box($data)
	{
		global $post;
		$post_id = $post->ID;
		if( isset( $data[RES_META_BOX_PREFIX.'offer_images'] ) ){

			$key = RES_META_BOX_PREFIX.'offer_images';

			update_post_meta( $post_id, $key, $data[RES_META_BOX_PREFIX.'offer_images'] );
			// save data

		}else{
			$key = RES_META_BOX_PREFIX.'offer_images';
			// delete data
			delete_post_meta( $post_id, $key );
		}
	}

	static function RES_crud_offers_features_meta_box($data)
	{
		global $post;
		$post_id = $post->ID;
		if( isset( $data[RES_META_BOX_PREFIX.'features'] ) ){
			$crud_data = $data[RES_META_BOX_PREFIX.'features'];
			$key = RES_META_BOX_PREFIX.'features';
			$crud_data = array_map( 'sanitize_text_field', $crud_data );
			update_post_meta( $post_id, $key, $crud_data );
		}else{
			$key = RES_META_BOX_PREFIX.'features';
			// delete data
			delete_post_meta( $post_id, $key );
		}
	}

	static function RES_get_option($key, $default = '')
	{
		$res_settings = get_option( 'res_settings' );

		if (isset($res_settings[$key]) && $res_settings[$key] != null)
		{
			return $res_settings[$key];
		}
		else
		{
			return $default;
		}
	}

	static function remove_from_array_keys($array_keys, $array)
	{

		foreach ($array_keys as $key)
		{
			unset($array[$key]);
		}

		return $array;
	}

	static function render_field($field, $key, $value)
	{
		?>
			<? if($field['type'] == 'text' || $field['type'] == 'number'): ?>
				<input id="<?=$key?>" class="form-control input-sm" type="<?=$field['type']?>" name="<?=$key?>" value="<?=$value?>">
			<? elseif ($field['type'] == 'textarea') : ?>
				<textarea name="<?=$key?>" id="<?=$key?>" rows="5"><?=$value?></textarea>
			<? elseif ($field['type'] == 'select') : ?>
				<select name="<?=$key?>" id="<?=$key?>">
					<? foreach ($field['options'] as $option) : ?>
						<option <? if($option == $value) echo 'selected'; ?> value="<?=$option?>"><?=$option?></option>
					<? endforeach; ?>
				</select>
			<? elseif ($field['type'] == 'upload_files') : ?>
				<div class="input-group">
					<textarea id="<?php echo $field['key']; ?>" name="<?=$key?>" class="form-control custom-control place-attachment" rows="2" style="resize:none;"><?php echo stripcslashes($value); ?></textarea>
					<span class="upload-attachment input-group-addon btn btn-info">UPLOAD</span>
				</div>
			<? endif; ?>
		<?php
	}
}