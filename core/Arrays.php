<?php

class Arrays
{
	static function main_meta_box_array()
	{
		return array(

			'price' => [
				'title' => 'Price',
				'type'  => 'number'
			],

			'offer_type' => [
				'title' => 'Offer type',
				'type'  => 'select',
				'options' => array (
					'House',
					'Vila',
					'Office',
					'Duplex',
					'Retail'
				)
			],

			'purpose' => [
				'title' => 'Purpose',
				'type'  => 'select',
				'options' => array (
					'Rent',
					'Sell'
				)
			],

			'status' => [
				'title' => 'Status',
				'type'  => 'select',
				'options' => array (
					'Normal',
					'Available',
					'Not available',
					'Sold',
					'Open house'
				)
			],

			'bedrooms' => [
				'title' => 'Bedrooms',
				'type'  => 'number'
			],

			'bathrooms' => [
				'title' => 'Bathrooms',
				'type'  => 'number'
			],

			'area' => [
				'title' => 'Area',
				'type'  => 'text'
			],

			'address' => [
				'title' => 'Address',
				'type'  => 'text'
			],

			'city' => [
				'title' => 'City',
				'type'  => 'text'
			],

			'country' => [
				'title' => 'Country',
				'type'  => 'text'
			],

			'latitude' => [
				'title' => 'Latitude',
				'type'  => 'text'
			],

			'longitude' => [
				'title' => 'Longitude',
				'type'  => 'text'
			],

			'iframes' => [
				'title' => 'Offer\'s iframes',
				'type' => 'textarea'
			],

			'attachment_files' => [
				'title' => 'Upload files (One id per line)',
				'type' => 'upload_files'
			]

		);
	}

	static function res_settings_array()
	{
		return array(

			'default_map_latitude' => [
				'title' => 'Default map latitude',
				'type'  => 'text'
			],

			'default_map_longitude' => [
				'title' => 'Default map longitude',
				'type'  => 'text'
			],

			'map_zoom' => [
				'title' => 'Map zoom',
				'type'  => 'number'
			]
		);
	}

	static function res_agency_array()
	{
		return array(

			'email' => [
				'title' => 'Agency email',
				'type'  => 'text'
			],

			'fax' => [
				'title' => 'Agency fax',
				'type'  => 'text'
			],

			'mobile' => [
				'title' => 'Agency mobile',
				'type'  => 'text'
			],

			'city' => [
				'title' => 'Agency city',
				'type'  => 'text'
			],

			'purpose' => [
				'title' => 'Agency urpose',
				'type'  => 'select',
				'options' => array (
					'Any purpose',
					'Rent',
					'Sell'
				)
			],
		);
	}
}