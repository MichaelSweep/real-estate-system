<?php

require_once( RES_PATH.'/core/Helpers.php' );
require_once( RES_PATH.'/core/Arrays.php' );
require_once( RES_PATH.'/core/RegisterRole.php' );


class RegisterOffer {

	// options
	const SETTINGS_OPTION = 'res_settings';

	/**
	 * Class plugin RegisterOffer constructor
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function __construct()
    {
	    // Offer custom post register
		add_action( 'init',             array($this, 'register_custom_post_offers' ), 0);
		add_action( 'init',             array($this, 'register_taxonomies'), 0 );

		// Metaboxes
		add_action( 'add_meta_boxes',   array($this, 'res_add_main_meta_box_section' ) );
		add_action( 'add_meta_boxes',   array($this, 'res_add_map_meta_box_section' ) );
		add_action( 'add_meta_boxes',   array($this, 'res_add_photos_meta_box_section' ) );
		add_action( 'add_meta_boxes',   array($this, 'res_add_features_meta_box_section' ) );
		add_action( 'save_post',        array($this, 'res_save_meta_fields') );
		add_action( 'new_to_publish',   array($this, 'res_save_meta_fields') );

		// Show more offers
		add_action( 'wp_footer',                        array($this, 'show_more_offers_action_javascript'));
		add_action( 'wp_ajax_show_more_offers',         array($this, 'show_more_offers_action_callback') );
		add_action( 'wp_ajax_nopriv_show_more_offers',  array($this, 'show_more_offers_action_callback') );

		// Filter offer search
		add_action( 'wp_footer',                                array($this, 'filter_offers_action_javascript'));
		add_action( 'wp_ajax_nopriv_filter_offers',             array($this, 'filter_offers_action_callback') );
		add_action( 'wp_ajax_filter_offers',                    array($this, 'filter_offers_action_callback') );
	    add_action( 'wp_footer',                                array($this, 'show_more_filter_offers_action_javascript'));
	    add_action( 'wp_ajax_show_more_filter_offers',          array($this, 'show_more_filter_offers_action_callback') );
	    add_action( 'wp_ajax_nopriv_show_more_filter_offers',   array($this, 'show_more_filter_offers_action_callback') );

		// Map offer search
		add_action( 'wp_ajax_nopriv_map_search_offers', array($this, 'search_map_offers_action_callback') );
		add_action( 'wp_ajax_map_search_offers',        array($this, 'search_map_offers_action_callback') );
	}

	/**
	 * Register custom post offers
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function register_custom_post_offers()
	{
		register_post_type( 'offer',
			array(
				'show_in_menu'      => true,
				'menu_position'     => 6,
				'menu_icon'         => 'dashicons-admin-home',
				'menu_name'         => 'Offers',
				'taxonomies'        => array('Offer'),
				'public'            => true,
				'has_archive'       => true,
				'show_ui'			=> true,
				'can_export'        => true,
				'_builtin'			=> false,
				'exclude_from_search'=> false,
				'capability_type'	=> array('offer', 'offers'),
				'supports'          => array( 'title', 'editor','thumbnail', 'author', 'excerpt'),
				'labels'            => array (
					'name'          => __( 'Real estate system' ),
					'singular_name' => __( 'Offers' ),
					'edit_item'         => __( 'Edit offer' ),
					'update_item'       => __( 'Update offer' ),
					'add_new_item'      => __( 'Add New offer' ),
					'new_item_name'     => __( 'New offers Name' ),
					'search_items'      => __( 'Search Offer' ),
					'not_found'         => __( 'No Offer found' ),
					'not_found_in_trash'=> __( 'No Offer found in Trash' ),
				)
			)
		);
	}

	/**
	 * Register taxonomies for offer's posts
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function register_taxonomies()
	{
		$labels = array(
			'name'              => _('Offers'),
			'singular_name'     => _('Offers'),
			'search_items'      => __( 'Search offers categories' ),
			'all_items'         => __( 'All offers categories' ),
			'parent_item'       => __( 'Parent offers category' ),
			'parent_item_colon' => __( 'Parent offers category:' ),
			'edit_item'         => __( 'Edit offers category' ),
			'update_item'       => __( 'Update offers category' ),
			'add_new_item'      => __( 'Add New offers category' ),
			'new_item_name'     => __( 'New offers category name' ),
			'menu_name'         => __( 'Offers Categories' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'rewrite'           => array( 'slug' => 'offers' ),
		);
		register_taxonomy( 'Offer', array( 'offer' ), $args );
	}

	/**
	 * Add main meta box section
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function res_add_main_meta_box_section()
    {
		add_meta_box(
			'res_custom_meta_box',       // $id
			'Offer features',                  // $title
			array('AdminController', 'get_main_meta_box_section'),  // $callback
			'offer',                 // $page
			'normal',                  // $context
			'high'                     // $priority
		);
	}

	/**
	 * Add map meta box section
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function res_add_map_meta_box_section()
    {
		add_meta_box(
			'res_map_meta_box',       // $id
			'Map',                  // $title
			array('AdminController', 'get_map_meta_box_section'),  // $callback
			'offer',                 // $page
			'normal',                  // $context
			'high'                     // $priority
		);
	}

	/**
	 * Add photos meta box section
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function res_add_photos_meta_box_section()
    {
		add_meta_box(
			'res_photos_meta_box',       // $id
			'Offers photos',                  // $title
			array('AdminController', 'get_photos_meta_box_section'),  // $callback
			'offer',                 // $page
			'normal',                  // $context
			'low'                     // $priority
		);
	}

	/**
	 * Add features meta box section
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function res_add_features_meta_box_section()
    {
		add_meta_box(
			'res_features_meta_box',       // $id
			'Offer features',                  // $title
			array('AdminController', 'get_features_meta_box_section'),  // $callback
			'offer',                 // $page
			'normal',                  // $context
			'low'                     // $priority
		);
	}

	/**
	 * Save data from main meta box section
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function res_save_meta_fields()
	{
		global $post;
		$post_id = $post->ID;

		// verify nonce
		if( !isset( $_POST['main_meta_box_section_nonce'] ) || !wp_verify_nonce( $_POST['main_meta_box_section_nonce'], 'my_main_meta_box_section_nonce' ) )
			return 'nonce not verified';

		// check autosave
		if ( wp_is_post_autosave( $post_id ) )
			return 'autosave';

		//check post revision
		if ( wp_is_post_revision( $post_id ) )
			return 'revision';

		// check permissions
		if ( ! current_user_can( 'edit_offer', $post_id ) ) {
			return 'cannot edit page';
		} elseif ( ! current_user_can( 'edit_offer', $post_id ) ) {
			return 'cannot edit post';
		}

		if ( $_POST['post_type'] == 'offer' ) {
			foreach (Arrays::main_meta_box_array() as $key=>$field)
			{
				Helpers::RES_crud_offers_meta_box($key,$field);
			}

			Helpers::RES_crud_offers_photos_meta_box($_POST);

			Helpers::RES_crud_offers_features_meta_box($_POST);
		}
	}

	/**
	 * Add to footer script that downloads new offers by clicking on show more offers button
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function show_more_offers_action_javascript()
    { ?>
		<script>
            jQuery('#show_more_offers').on('click', function () {
                var page_number = parseInt(jQuery('#show_more_offers').attr('page-number'));
                var data = {
                    action: 'show_more_offers',
	                page_number: page_number
                };

                jQuery.post( ajaxurl, data, function(response) {
                    jQuery('#show_more_offers').attr('page-number', page_number + 1);
                    if(response !== '')
                    {
                        jQuery('#offers-list').append(response);
                    }
                    else
                    {
                        jQuery('#show_more_offers').html('No posts');
                    }
                });
            });
		</script>
	<?php }

	/**
	 * Callback on show more offers request
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function show_more_offers_action_callback()
    {
		global $post;
		$page_number = intval( $_POST['page_number'] );
		$posts_per_page = 1;

		$args = array(
			'post_type'   => 'offer',
			'post_status' => 'publish',
			'posts_per_page' => $posts_per_page,
			'offset' => $page_number * $posts_per_page
		);

		$offers = new WP_Query( $args );
		$return = '';

		if( $offers->have_posts() ) :
			foreach( $offers->get_posts() as $offer ) :
				setup_postdata($offer);
				$offers->the_post(); ?>
				<div class="col-4">
					<div class="offer-item">
						<a href="<?=get_the_permalink()?>">
							<?=the_post_thumbnail()?>
						</a>
						<div class="offer-description">
							<a href="<?=get_the_permalink()?>">
								<?=the_title()?>
							</a>
							<p><?=$post->res_price?> / mon</p>
							<p><?= wp_trim_words (get_the_content(), 10 )  ?></p>
							<p>Bedrooms: <?= $post->res_bedrooms ?></p>
							<p>Bathrooms: <?= $post->res_bathrooms ?></p>
							<p>Area: <?= $post->res_area ?></p>
						</div>
					</div>
				</div>
			<? endforeach; ?>
			<? wp_reset_postdata(); ?>
			<?php
		endif;

		wp_die();
	}

	/**
	 * Adding filter offers javascript to footer
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function filter_offers_action_javascript()
    { ?>
		<script>
            jQuery( '#offers_filter #offers_filter_submit' ).on('click', function () {
                var data = 'action=filter_offers&' + jQuery( '#offers_filter' ).serialize();

                console.log(data);
                jQuery('#offers_filter_show_more').attr('page-number', 1);
                jQuery('#offers_filter_show_more').html('Load more');

                jQuery.post( ajaxurl, data, function(response) {
                    if(response !== '')
                    {
                        jQuery('#filter_offers').html(response);
                    }
                    else
                    {
                        //jQuery('#show_more_offers').html('No posts');
                    }
                });
            });
		</script>
	<?php }

	/**
	 * Callback on filter's request
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function filter_offers_action_callback()
    {
		global $post;
	    $args = $this->res_offer_filter_args($_POST, 1);
		$offers = new WP_Query( $args );
		if( $offers->have_posts() ) :
			foreach( $offers->get_posts() as $offer ) :
				setup_postdata($offer);
				$offers->the_post(); ?>
				<div class="col-4">
					<div class="offer-item">
						<a href="<?=get_the_permalink()?>">
							<?=the_post_thumbnail()?>
						</a>
						<div class="offer-description">
							<a href="<?=get_the_permalink()?>">
								<?=the_title()?>
							</a>
							<p><?=$post->res_price?> / mon</p>
							<p><?= wp_trim_words (get_the_content(), 10 )  ?></p>
							<p>Bedrooms: <?= $post->res_bedrooms ?></p>
							<p>Bathrooms: <?= $post->res_bathrooms ?></p>
							<p>Area: <?= $post->res_area ?></p>
						</div>
					</div>
				</div>
			<? endforeach; ?>
			<? wp_reset_postdata(); ?>
			<?php
        else :
            echo 'No offers to this filter';
		endif;
		wp_die();
	}

	/**
	 * Add to footer script that downloads new offers by clicking on show more offers button to filter block
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function show_more_filter_offers_action_javascript()
	{ ?>
        <script>
            jQuery('#offers_filter_show_more').on('click', function () {
                var page_number = parseInt(jQuery('#offers_filter_show_more').attr('page-number'));
                var data = 'action=show_more_filter_offers&page_number=' + page_number + '&' + jQuery( '#offers_filter' ).serialize();

                jQuery.post( ajaxurl, data, function(response) {
                    jQuery('#offers_filter_show_more').attr('page-number', page_number + 1);
                    if(response !== '')
                    {
                        jQuery('#filter_offers').append(response);
                    }
                    else
                    {
                        jQuery('#offers_filter_show_more').html('No posts');
                    }
                });
            });
        </script>
	<?php }

	/**
	 * Callback on show more offers request
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function show_more_filter_offers_action_callback()
	{
		global $post;
		$page_number = intval( $_POST['page_number'] );
		$posts_per_page = 1;

		$args = $this->res_offer_filter_args($_POST, $posts_per_page, $page_number);

		$offers = new WP_Query( $args );

		if( $offers->have_posts() ) :
			foreach( $offers->get_posts() as $offer ) :
				setup_postdata($offer);
				$offers->the_post(); ?>
                <div class="col-4">
                    <div class="offer-item">
                        <a href="<?=get_the_permalink()?>">
							<?=the_post_thumbnail()?>
                        </a>
                        <div class="offer-description">
                            <a href="<?=get_the_permalink()?>">
								<?=the_title()?>
                            </a>
                            <p><?=$post->res_price?> / mon</p>
                            <p><?= wp_trim_words (get_the_content(), 10 )  ?></p>
                            <p>Bedrooms: <?= $post->res_bedrooms ?></p>
                            <p>Bathrooms: <?= $post->res_bathrooms ?></p>
                            <p>Area: <?= $post->res_area ?></p>
                        </div>
                    </div>
                </div>
			<? endforeach; ?>
			<? wp_reset_postdata(); ?>
			<?php
		endif;

		wp_die();
	}

	/**
	 * Callback on filter's request
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function search_map_offers_action_callback()
    {
		global $post;
		$args = $this->res_offer_filter_args($_POST, -1);
		$offers = new WP_Query( $args );
	    $marker_icon = apply_filters( 'res_maps_marker_icon', RES_URL.'/assets/pin-marker.png' );
	    ?>
        <script>
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers = [];
        </script>
	    <?
		if( $offers->have_posts() ) :
			foreach( $offers->get_posts() as $offer ) :
				setup_postdata($offer);
				$offers->the_post(); ?>
                <script>
                    var marker_<?=$offer->ID?> = new google.maps.Marker({
                        position: new google.maps.LatLng(<?php echo $offer->res_latitude; ?>, <?php echo $offer->res_longitude; ?>),
                        map: map,
                        icon: '<?php echo $marker_icon; ?>'
                    });

                    var contentString_<?=$offer->ID?> =
                        '<div id="content">'+
                        '<div id="siteNotice"></div>'+
                        '<h1 id="firstHeading" class="firstHeading"><a href="<?=get_permalink($offer->ID)?>"><?=$offer->post_title?></a></h1>'+
                        '<div id="bodyContent">'+
                        '<?=wp_trim_words($offer->post_content,20);?>'+
                        '<br>Price: <?=$offer->res_price?> / month' +
                        '<br>Type: <?=$offer->res_offer_type?>' +
                        '<br>Purpose: <?=$offer->res_purpose?>' +
                        '</div>'+
                        '</div>';

                    var infowindow_<?=$offer->ID?> = new google.maps.InfoWindow({
                        content: contentString_<?=$offer->ID?>
                    });

                    marker_<?=$offer->ID?>.addListener('click', function() {
                        infowindow_<?=$offer->ID?>.open(map, marker_<?=$offer->ID?>);
                    });

                    map.setCenter(marker_<?=$offer->ID?>.position);
                    markers.push(marker_<?=$offer->ID?>);
                </script>
			<? endforeach; ?>
			<? wp_reset_postdata(); ?>
			<?php
		endif;
		wp_die();
	}

	/**
	 * Return arguments for retrieving offers by filter query
	 *
	 * @since 1.0.0
     * @param array $post_data
     * @param integer $posts_per_page
	 * @return array
	 */
	function res_offer_filter_args($post_data, $posts_per_page, $page_number = 0)
	{
		$res_price_from = $post_data['res_price_from'];
		$res_price_to = $post_data['res_price_to'];
		if($res_price_to == 0)
		{
			$res_price_to = PHP_INT_MAX;
		}
		$res_city = $post_data['res_city'];
		$res_offer_type = $post_data['res_offer_type'];
		$res_purpose = $post_data['res_purpose'];
		$res_bedrooms = $post_data['res_bedrooms'];
		$res_bathrooms = $post_data['res_bathrooms'];

		return array(
			'post_type'   => 'offer',
			'post_status' => 'publish',
			'posts_per_page' => $posts_per_page,
			'offset' => $page_number * $posts_per_page,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'res_price',
					'value' => array ($res_price_from, $res_price_to),
					'compare' => 'BETWEEN',
					'type'    => 'numeric'
				),
				array(
					'key' => 'res_city',
					'value' => $res_city,
					'compare' => 'LIKE',
				),
				array(
					'key' => 'res_offer_type',
					'value' => $res_offer_type,
					'compare' => 'LIKE',
				),
				array(
					'key' => 'res_purpose',
					'value' => $res_purpose,
					'compare' => 'LIKE',
				),
				array(
					'type'    => 'numeric',
					'key' => 'res_bedrooms',
					'compare' => '>=',
					'value' => $res_bedrooms
				),
				array(
					'type'    => 'numeric',
					'key' => 'res_bathrooms',
					'compare' => '>=',
					'value' => $res_bathrooms
				)
			)
		);
	}
}