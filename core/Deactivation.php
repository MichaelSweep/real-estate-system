<?php

require_once( RES_PATH.'/core/RegisterRole.php' );

class Deactivation {

    // tables
    const SETTINGS_TABLE = 'wp_real_estate_system_settings';
    const SETTINGS_OPTION = 'res_settings';

    /**
     * Class plugin Deactivation constructor
     *
     * @since 1.0.0
     * @return void
     */
    public function __construct()
    {

    }

    static function plugin_deactivation()
    {
	    delete_option(self::SETTINGS_OPTION);
	    RegisterRole::remove_role_capabilities();
    }
}