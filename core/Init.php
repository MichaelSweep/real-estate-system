<?php

require_once( RES_PATH.'/core/RegisterRole.php' );
require_once( RES_PATH.'/core/RegisterAgency.php' );

class Init {

	const SETTINGS_OPTION = 'res_settings';

    /**
     * Class plugin Init constructor
     *
     * @since 1.0.0
     * @return void
     */
    public function __construct()
    {
		self::res_assets();
	    add_action( 'wp_enqueue_scripts',   array($this, 'res_assets' ) );
	    add_action( 'admin_menu',           array($this, 'register_menu') );
	    add_action( 'plugins_loaded',       array($this, 'res_load_plugin_textdomain'));
	    add_action( 'init',                 array('RegisterAgency', 'register_agency'));
	    add_action( 'admin_head',           array('RegisterAgency', 'wp_user_groups_admin_assets'));
	    add_filter( 'wp_user_profiles_sections', array('RegisterAgency', 'wp_user_groups_add_profile_section'));
	    add_action( 'wp_head',              array($this, 'myplugin_ajaxurl'));
    }

	/**
	 * Add plugin data to system after activation
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static function plugin_activation()
	{
		add_option(self::SETTINGS_OPTION);
		RegisterRole::add_offer_agent_role();
		RegisterRole::set_role_capabilities();
	}

	/**
	 * Register admin menu
	 *
	 * @since 1.0.0
	 * @return void
	 */
    static function register_menu()
    {
	    add_submenu_page(
		    'edit.php?post_type=offer',
		    'Settings',
		    'Settings',
		    'manage_options',
		    'real-estate-system-settings',
		    array('AdminController','get_and_save_settings_page')
	    );
    }

	/**
	 * Add localization language
	 *
	 * @singce 1.0.0
	 * @return void
	 **/
	function res_load_plugin_textdomain()
	{
		load_plugin_textdomain( 'real-estate-system', FALSE, basename( RES_PATH ) . '/langs/' );
	}

	/**
	 * Adding assets to real estate system
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function res_assets()
	{
		wp_enqueue_style( 'res-style-css', RES_URL . '/assets/style.css' );
		wp_enqueue_script( 'res-script-js',RES_URL . '/assets/script.js', array('jquery'));
	}

	/**
	 * Make able to add ajaxurl link
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function myplugin_ajaxurl() {
		echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
	}
}