<?php
/**
 * Plugin Name: Real Estate System
 * Plugin URI:
 * Description: System for real estate website
 * Version: 1.0.0
 * Author: Mikhail
 * Author URI:
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: real-estate-system
 * Domain Path:
 */

// define pathes
define('RES_PATH', untrailingslashit(plugin_dir_path( __FILE__ )) );
define('RES_URL',  untrailingslashit(plugin_dir_url( __FILE__ )) );
define('RES_META_BOX_PREFIX', 'res_' );
define('AGENT_CAPS' , serialize (array(
	'edit_offer',
	'edit_offers',
	'read_offer',
	'delete_offer',
	'delete_offers',
	'publish_offers',
	'create_offers',
	'upload_files',
	'edit_published_offers',
	'read_private_offers',
	'delete_private_offers',
	'delete_published_offers',
	'edit_others_offers'
)));


// core
require_once( RES_PATH.'/core/Init.php' );
require_once( RES_PATH.'/core/Deactivation.php' );
require_once( RES_PATH.'/core/RegisterOffer.php' );
require_once( RES_PATH.'/core/RegisterRole.php' );
require_once( RES_PATH.'/core/RegisterAgency.php' );

// admin
require_once( RES_PATH.'/admin/AdminController.php' );

/**
 * Initializing and deactivation functions
 *
 * @since 1.0.0
 */
if( class_exists('Init') && class_exists('Deactivation'))
{
    $res_init = new Init();
	register_activation_hook(__FILE__, array('Init', 'plugin_activation'));

	$res_deactivation = new Deactivation();
	register_deactivation_hook(__FILE__, array('Deactivation', 'plugin_deactivation'));
}

/**
 * Custom post initalization
 *
 * @since 1.0.0
 */
if( class_exists('RegisterOffer'))
{
	$res_register_offer = new RegisterOffer();
}

/**
 * Custom role initalization
 *
 * @since 1.0.0
 */
if( class_exists('RegisterRole'))
{
	$res_register_role = new RegisterRole();
}

/**
 * Custom user taxonomy agency initalization
 *
 * @since 1.0.0
 */
if( class_exists('RegisterRole'))
{
	$res_register_agency = new RegisterAgency();
}