jQuery( document ).ready(function() {
    jQuery("#add_feature").on('click',function (event) {
        var input =
            '<div class="feature_item">'
                + '<input class="form-control input-sm"'
                + 'type="text" name="res_features[]"'
                + 'value="">'
                + '<a onclick="delete_feature_event(jQuery(this))">Delete</a>'
            + '</div>';
        jQuery(".features_list").prepend(input);
    });

    jQuery("#offers_filter_submit").click();
});

function delete_feature_event(button) {
    jQuery(button).closest('.feature_item').remove();
}

var res_attachments;

jQuery('.upload-attachment').live('click', function( event ){

    event.preventDefault();
    var text_field = jQuery(this).closest('div').find('.place-attachment');

    // var parent = jQuery(this).closest('.tab-content').find('.thumbs-prev');
    // Create the media frame.
    res_attachments = wp.media.frames.res_attachments = wp.media({
        title: 'Select attachment for property',
        button: {
            text: 'Add',
        },
        multiple: true  // Set to true to allow multiple files to be selected
    });

    // When an image is selected, run a callback.
    res_attachments.on( 'select', function() {
        // We set multiple to false so only get one image from the uploader
        var selection = res_attachments.state().get('selection');
        selection.map( function( attachment ) {
            attachment = attachment.toJSON();
            if (text_field.val() != '') {
                text_field.val( text_field.val() + '\n'+attachment.id);
            } else {
                text_field.val( text_field.val() +attachment.id);
            }

        });
    });

    // Finally, open the modal
    res_attachments.open();
});